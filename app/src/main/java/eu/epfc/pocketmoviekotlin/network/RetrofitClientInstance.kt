package eu.epfc.pocketmoviekotlin.network

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitClientInstance {

    private var BASE_URL:String = TmdbApi.baseURL
    val getClient: RetrofitTmdbApi
        get() {

            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()

            return retrofit.create(RetrofitTmdbApi::class.java)

        }

}