package eu.epfc.pocketmoviekotlin.network

/**
 * Class used to communicate errors to the views when a http request fails
 */
class ConnectionResultStatus(val result: ConnectionResult) {

    enum class ConnectionResult {
        Uninitialized,
        Success,
        ErrorNoConnection,
        ErrorServerInternal
    }

}