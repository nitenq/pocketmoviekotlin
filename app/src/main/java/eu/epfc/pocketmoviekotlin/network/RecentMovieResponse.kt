package eu.epfc.pocketmoviekotlin.network

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by kent1190 on 26/04/2018.
 */

class RecentMovieResponse {


    @SerializedName("results")
    @Expose
    var results: List<Result>? = null

    inner class Result {

        @SerializedName("poster_path")
        @Expose
        var posterPath: String? = null

        @SerializedName("overview")
        @Expose
        var overview: String? = null
        @SerializedName("release_date")
        @Expose
        var releaseDate: String? = null
        @SerializedName("genre_ids")
        @Expose
        var genreIds: List<Int>? = null
        @SerializedName("id")
        @Expose
        var id: Int? = null
        @SerializedName("title")
        @Expose
        var title: String? = null
        @SerializedName("vote_average")
        @Expose
        var voteAverage: Double? = null

    }

}



