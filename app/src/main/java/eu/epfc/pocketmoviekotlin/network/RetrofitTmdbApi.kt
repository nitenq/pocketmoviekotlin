package eu.epfc.pocketmoviekotlin.network

import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.Call
import retrofit2.http.Path

interface RetrofitTmdbApi {

    @GET("/3/movie/popular")
    fun getRecentMovies(@Query("page") page : Int, @Query("api_key") apiKey : String) : Call<RecentMovieResponse>
    @GET("/3/movie/{filmId}")
    fun getDetailedMovie(@Path("filmId") filmId : String,  @Query("api_key") apiKey : String) : Call<DetailMovieResponse>
    @GET("/3/movie/{filmId}/videos")
    fun getYoutubeVideoId(@Path("filmId") filmId : String, @Query("api_key") apiKey : String) : Call<MovieVideosResponse>
}