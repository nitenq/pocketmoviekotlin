package eu.epfc.pocketmoviekotlin.network

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class DetailMovieResponse {

    @SerializedName("backdrop_path")
    @Expose
    var backdropPath : String? = null
    @SerializedName("genres")
    @Expose
    var genres : List<Genre>? = null
    @SerializedName("id")
    @Expose
    var movieId : Int ? = null
    @SerializedName("original_title")
    @Expose
    var originalTitle : String? = null
    @SerializedName("overview")
    @Expose
    var overview : String? = null
    @SerializedName("release_date")
    @Expose
    var  releaseDate : String? = null
    @SerializedName("title")
    @Expose
    var title : String? = null
    @SerializedName("vote_average")
    @Expose
    var voteAverage : Double? = 0.0

    inner class Genre {
        @SerializedName("name")
        @Expose
        var name: String? = null

    }

}
