package eu.epfc.pocketmoviekotlin.network

object TmdbApi {

    const val baseURL ="https://api.themoviedb.org/"
    val apiKey = "ea2dcee690e0af8bb04f37aa35b75075"
    private val baseImageURL ="https://image.tmdb.org/"

    fun getImageUrl(imagePath : String) : String {
        return baseImageURL + "t/p/w500/" + imagePath
    }
}