package eu.epfc.pocketmoviekotlin.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import eu.epfc.pocketmoviekotlin.model.Movie
import eu.epfc.pocketmoviekotlin.model.MovieManager
import eu.epfc.pocketmoviekotlin.network.TmdbApi
import eu.epfc.pocketmoviekotlin.R

class RecentMoviesAdapter : RecyclerView.Adapter<RecentMoviesAdapter.MovieViewHolder> {

    enum class ItemType(val value : Int) {
        MOVIE_ITEM (0),
        NEXT_PAGE_ITEM(1)
    }

    interface ListItemClickListener {
        fun onListItemClick(clickedItemIndex : Int)
    }

    private val listItemClickListener : ListItemClickListener

    constructor(listItemClickListener : ListItemClickListener){
        this.listItemClickListener = listItemClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {

        // get a layoutInflater from the context attached to the parent view
        val layoutInflater = LayoutInflater.from(parent.context)

        val itemView : View = when (viewType){
            // this it a movieItem
            ItemType.MOVIE_ITEM.value -> layoutInflater.inflate(R.layout.item_movie,parent,false)
            // this is a "next page" item
            else -> layoutInflater.inflate(R.layout.item_next_page,parent,false)
        }
        return MovieViewHolder(itemView = itemView)
    }

    override fun getItemCount(): Int {

        return (MovieManager.popularMovies.value?.size ?: 0) + 1

    }

    override fun getItemViewType(position: Int): Int {

        val movieList : List<Movie>? = MovieManager.popularMovies.value

        if (movieList != null) {
            return when (position){
                movieList.size -> ItemType.NEXT_PAGE_ITEM.value
                else -> ItemType.MOVIE_ITEM.value
            }
        }
        return  0
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {

        val itemViewGroup = holder.itemView as ViewGroup

        if (holder.itemViewType == ItemType.MOVIE_ITEM.value)
        {
            val titleTextView = itemViewGroup.findViewById<TextView>(R.id.text_title)
            val movieThumbImageView = itemViewGroup.findViewById<ImageView>(R.id.image_moviethumb)
            val ratingTextView = itemViewGroup.findViewById<TextView>(R.id.text_rating)

            val movieList : List<Movie>? = MovieManager.popularMovies.value

            val movie = movieList?.get(position)
            if (movie != null) {
                titleTextView.text = movie.title
                val rating = movie.voteAverage
                ratingTextView.text = "Rating : $rating"
                val thumbURL = TmdbApi.getImageUrl(movie.imagePath)
                Picasso.get().load(thumbURL).into(movieThumbImageView)
            }
        }
    }

    inner class MovieViewHolder : RecyclerView.ViewHolder, View.OnClickListener {

        override fun onClick(view: View?) {
            val clickedPosition = adapterPosition
            listItemClickListener.onListItemClick(clickedPosition)
        }

        constructor(itemView : View) : super(itemView) {
            itemView.setOnClickListener(this)
        }
    }
}