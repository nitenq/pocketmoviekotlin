package eu.epfc.pocketmoviekotlin.ui


import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import eu.epfc.pocketmoviekotlin.model.Movie
import eu.epfc.pocketmoviekotlin.model.MovieManager
import eu.epfc.pocketmoviekotlin.network.ConnectionResultStatus
import android.os.Parcelable
import eu.epfc.pocketmoviekotlin.R


class RecentMoviesFragment : Fragment(), RecentMoviesAdapter.ListItemClickListener {

    private lateinit var popularMoviesRecyclerView : RecyclerView
    private var recentMoviesAdapter : RecentMoviesAdapter? = null
    private lateinit var linearLayoutManager : LinearLayoutManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_recent_movies, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        linearLayoutManager = LinearLayoutManager(activity)

        // restore state in case of configuration change
        if (savedInstanceState != null) {
            val state : Parcelable ? = savedInstanceState.getParcelable("RecyclerViewLayoutManagerState")
            if (state != null) {
                linearLayoutManager.onRestoreInstanceState(state)
            }
        }
    }

    override fun onStart() {
        super.onStart()

        if (recentMoviesAdapter == null){
            // set the adapter of the RecyclerView
            popularMoviesRecyclerView = view!!.findViewById(R.id.recyclerview_recentMovie)
            recentMoviesAdapter = RecentMoviesAdapter(this)
            popularMoviesRecyclerView.adapter = recentMoviesAdapter
        }

        //notify the adapter that the data have changed
        recentMoviesAdapter?.notifyDataSetChanged()

        popularMoviesRecyclerView.layoutManager = linearLayoutManager

        //observe errors returned by http requests
        MovieManager.popularMoviesStatus.observe(this, Observer { connectionStatus ->

            // avoid to hide the progress bar when the first LiveData onChange is called (initialization)
            if (connectionStatus.result != ConnectionResultStatus.ConnectionResult.Uninitialized){
                val progressBar : ProgressBar = view!!.findViewById(R.id.progressBar)
                progressBar.visibility = View.INVISIBLE
            }

            displayErrorMessageIfNeeded(connectionStatus)
        })

        // when the movies are updated (result of a request) ...
        MovieManager.popularMovies.observe(this, Observer {
            this.updatePopularMovies()
        })

        // request the movie list!
        MovieManager.popularMovies.value?.let  {movieList ->

            // we don't fetch new films if the list is already filled
            if (movieList.isEmpty()) {
                val progressBar : ProgressBar = view!!.findViewById(R.id.progressBar)
                progressBar.visibility = View.VISIBLE
                MovieManager.fetchPopularMovies()
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        // save the state of the recyclerView (i.e : its scroll position)
        outState.putParcelable("RecyclerViewLayoutManagerState", linearLayoutManager.onSaveInstanceState())
    }

    private fun displayErrorMessageIfNeeded(connectionStatus: ConnectionResultStatus){

        when (connectionStatus.result){
            ConnectionResultStatus.ConnectionResult.ErrorNoConnection -> {
                val errorMessage = getString(R.string.error_no_internet)
                val toast = Toast.makeText(context, errorMessage, Toast.LENGTH_LONG)
                toast.show()
            }
            ConnectionResultStatus.ConnectionResult.ErrorServerInternal -> {
                val errorMessage = getString(R.string.error_internal_server)
                val toast = Toast.makeText(context, errorMessage, Toast.LENGTH_LONG)
                toast.show()
            }
            else -> {

            }
        }
    }

    private fun updatePopularMovies(){

        // if the list is empty, we hide the recyclerView
        if (MovieManager.popularMovies.value.isNullOrEmpty()){
            popularMoviesRecyclerView.visibility = View.INVISIBLE
        }
        else
        {
            popularMoviesRecyclerView.visibility = View.VISIBLE
        }

        recentMoviesAdapter?.notifyDataSetChanged()

    }

    override fun onListItemClick(clickedItemIndex: Int) {

        val movieList : List<Movie>? = MovieManager.popularMovies.value
        if (movieList != null)
        {
            if (clickedItemIndex < movieList.size)
            {
                val movie = movieList[clickedItemIndex]
                val detailIntent = Intent(activity, DetailedMovieActivity::class.java)
                detailIntent.putExtra("movieId",movie.movieId)
                startActivity(detailIntent)
            }
            // else the user pushed the button "next"
            else
            {
                MovieManager.fetchPopularMovies()
            }
        }
    }
}
