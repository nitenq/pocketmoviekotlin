package eu.epfc.pocketmoviekotlin.ui

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.*
import eu.epfc.pocketmoviekotlin.model.Movie
import eu.epfc.pocketmoviekotlin.model.MovieManager
import androidx.lifecycle.Observer
import com.squareup.picasso.Picasso
import eu.epfc.pocketmoviekotlin.network.ConnectionResultStatus
import eu.epfc.pocketmoviekotlin.network.TmdbApi
import eu.epfc.pocketmoviekotlin.R

class DetailedMovieActivity : AppCompatActivity() {

    private var currentMovie : Movie? = null
    private lateinit var pocketCheckBox: CheckBox
    private lateinit var releaseAndGenreTextView: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detailed_movie)

        val movieId = intent.getIntExtra("movieId",0)

        currentMovie = MovieManager.getPopularMovieById(movieId)
        releaseAndGenreTextView = findViewById(R.id.text_release_length)
        pocketCheckBox = findViewById(R.id.checkBox_pocket)

        val titleTextView : TextView = findViewById(R.id.text_detaild_title)
        titleTextView.text = currentMovie?.title ?: ""

        // check if the movie is saved in the db and fill the checkbox
        val checkBox : CheckBox = findViewById(R.id.checkBox_pocket)
        checkBox.isChecked = MovieManager.isMovieSaved(movieId)

        // disable the video button if we don't have a valid youtubeId yet
        val videoButton : Button = findViewById(R.id.button_video)
        if (currentMovie?.youtubeVideoId == null)
        {
            videoButton.isEnabled = false
            videoButton.alpha = .5f
        }
        else
        {
            videoButton.isEnabled = true
            videoButton.alpha = 1.0f
        }
    }

    override fun onStart() {
        super.onStart()

        /*
        setup notifications when movie details are fetched from server and when youtube ids are fetched from server
        */


        //observe errors returned by http requests
        MovieManager.detailedMovieStatus.observe(this, Observer { connectionStatus ->

            displayErrorMessageIfNeeded(connectionStatus)
        })

        MovieManager.youtubeIdStatus.observe(this, Observer { connectionStatus ->

            displayErrorMessageIfNeeded(connectionStatus)
        })

        currentMovie?.let { currentMovie ->

            // observe movie details changes
            currentMovie.details.observe(this, Observer { movieDetails ->

                if (movieDetails != null) {
                    this.updateDetailedMovie(currentMovie)
                }
            })


            // observe movie youtube trailer changes
            currentMovie.youtubeVideoId.observe(this, Observer { youtubeVideoId ->
                if (youtubeVideoId != null) {
                    val videoButton: Button = findViewById(R.id.button_video)
                    videoButton.isEnabled = true
                    videoButton.alpha = 1.0f
                }
            })

            // if we don't have details about the movie yet (if it's the first time we display this activity)
            if (currentMovie.details.value == null) {
                // http request details
                MovieManager.fetchDetailedMovie(currentMovie.movieId)
            } else {
                updateDetailedMovie(currentMovie)
            }
            if (currentMovie.youtubeVideoId.value == null) {
                // http request youtube id
                MovieManager.fetchYoutubeVideoIds(currentMovie.movieId)
            }

            updateBasicMovieUI(currentMovie)

        }
    }

    override fun onStop() {
        super.onStop()
        MovieManager.detailedMovieStatus.value = ConnectionResultStatus(ConnectionResultStatus.ConnectionResult.Uninitialized)
        MovieManager.youtubeIdStatus.value = ConnectionResultStatus(ConnectionResultStatus.ConnectionResult.Uninitialized)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId== android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun displayErrorMessageIfNeeded(connectionStatus: ConnectionResultStatus){

        when (connectionStatus.result){
            ConnectionResultStatus.ConnectionResult.ErrorNoConnection -> {
                val errorMessage = getString(R.string.error_no_internet)
                val toast = Toast.makeText(applicationContext, errorMessage, Toast.LENGTH_LONG)
                toast.show()
            }
            ConnectionResultStatus.ConnectionResult.ErrorServerInternal -> {
                val errorMessage = getString(R.string.error_internal_server)
                val toast = Toast.makeText(applicationContext, errorMessage, Toast.LENGTH_LONG)
                toast.show()
            }
            else -> {

            }
        }
    }

    fun onClickPocketCheckbox(view : View) {

        val checkboxView = view as CheckBox
        if (currentMovie != null){
            if (!checkboxView.isChecked){
                MovieManager.removeMovieFromDatabase(currentMovie!!.movieId, applicationContext)
            }
            else {
                MovieManager.saveMovieInDatabase(currentMovie!!.movieId, applicationContext)
            }
        }
    }

    fun onClickVideoButton(@Suppress("UNUSED_PARAMETER") view : View){

        currentMovie?.let { currentMovie ->
            if (currentMovie.youtubeVideoId.value != null) {
                val youtubeId: String = currentMovie.youtubeVideoId.value!!
                val appIntent = Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:$youtubeId"))
                try {
                    startActivity(appIntent)
                } catch (ex: ActivityNotFoundException) {
                    val errorMessage = getString(R.string.error_youtube_not_installed)
                    val toast = Toast.makeText(applicationContext, errorMessage, Toast.LENGTH_LONG)
                    toast.show()
                }
            }
        }
    }


    private fun updateBasicMovieUI(movie : Movie)
    {
        val titleTextView : TextView = findViewById(R.id.text_detaild_title)
        titleTextView.text = movie.title

        val ratingTextView : TextView = findViewById(R.id.text_rating)
        ratingTextView.text = movie.voteAverage.toString()

        val overviewTextView : TextView = findViewById(R.id.text_overview)
        overviewTextView.text = movie.overView

        val releaseDateTextView : TextView = findViewById(R.id.text_release_length)
        releaseDateTextView.text = movie.releaseDate
    }

    private fun updateDetailedMovie(detailedMovie : Movie) {

        detailedMovie.details.value?.let { movieDetails ->

            val movieImageView: ImageView = findViewById(R.id.image_detailed_backdrop)
            val thumbURL = TmdbApi.getImageUrl(movieDetails.backdropPath)
            Picasso.get().load(thumbURL).into(movieImageView)

            val genreTextView : TextView = findViewById(R.id.text_genres)

            var genreString = ""
            for (genre in movieDetails.genreNames){
                genreString += genre
            }
            genreTextView.text = genreString
        }
    }
}
