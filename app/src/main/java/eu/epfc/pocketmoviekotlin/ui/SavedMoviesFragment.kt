package eu.epfc.pocketmoviekotlin.ui


import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import eu.epfc.pocketmoviekotlin.model.Movie
import eu.epfc.pocketmoviekotlin.model.MovieManager
import eu.epfc.pocketmoviekotlin.R


class SavedMoviesFragment : Fragment(), SavedMoviesAdapter.ListItemClickListener {

    private var savedMoviesAdapter : SavedMoviesAdapter? = null
    private lateinit var linearLayoutManager : LinearLayoutManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_saved_movies, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        linearLayoutManager = LinearLayoutManager(activity)

        // restore state in case of configuration change
        if (savedInstanceState != null) {
            val state : Parcelable? = savedInstanceState.getParcelable("RecyclerViewLayoutManagerState")
            if (state != null) {
                linearLayoutManager.onRestoreInstanceState(state)
            }
        }
    }

    override fun onStart() {
        super.onStart()

        val savedMoviesRecyclerView : RecyclerView = view!!.findViewById(R.id.recyclerview_saved_movies)

        // set the adapter of the RecyclerView
        if (savedMoviesAdapter == null){
            savedMoviesAdapter = SavedMoviesAdapter(this)
            savedMoviesRecyclerView.adapter = savedMoviesAdapter
        }

        activity?.let {activity ->
            MovieManager.loadAllMoviesFromDatabase(activity.applicationContext)
        }

        //notify the adapter that the data have changed
        savedMoviesAdapter?.notifyDataSetChanged()

        // if the list is empty, we show a text view
        val emptyListTextView : TextView = view!!.findViewById(R.id.text_empty_list)
        if (MovieManager.savedMovies.isEmpty()){
            savedMoviesRecyclerView.visibility = View.INVISIBLE
            emptyListTextView.visibility = View.VISIBLE
        }
        else{
            savedMoviesRecyclerView.visibility = View.VISIBLE
            emptyListTextView.visibility = View.INVISIBLE
        }


        savedMoviesRecyclerView.layoutManager = linearLayoutManager
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        // save the state of the recyclerView (i.e : its scroll position)
        outState.putParcelable("RecyclerViewLayoutManagerState", linearLayoutManager.onSaveInstanceState())
    }

    override fun onListItemClick(clickedItemIndex: Int) {
        val movieList : List<Movie>? = MovieManager.savedMovies
        if (movieList != null)
        {
            if (clickedItemIndex < movieList.size)
            {
                val movie = movieList[clickedItemIndex]
                val detailIntent = Intent(activity, DetailedMovieActivity::class.java)
                detailIntent.putExtra("movieId",movie.movieId)
                startActivity(detailIntent)
            }
        }
    }
}
