package eu.epfc.pocketmoviekotlin.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import eu.epfc.pocketmoviekotlin.model.MovieManager
import eu.epfc.pocketmoviekotlin.network.TmdbApi
import eu.epfc.pocketmoviekotlin.R

class SavedMoviesAdapter : RecyclerView.Adapter<SavedMoviesAdapter.MovieViewHolder>{

    interface ListItemClickListener {
        fun onListItemClick(clickedItemIndex : Int)
    }

    private val listItemClickListener : ListItemClickListener

    constructor(listItemClickListener : ListItemClickListener){

        this.listItemClickListener = listItemClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {

        // get a layoutInflater from the context attached to the parent view
        val layoutInflater = LayoutInflater.from(parent.context)

        // inflate the layout item_planet in a view
        val itemView = layoutInflater.inflate(R.layout.item_movie,parent,false)

        return MovieViewHolder(itemView = itemView)

    }

    override fun getItemCount(): Int {

        return MovieManager.savedMovies.size
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {

        val itemViewGroup = holder.itemView as ViewGroup
        val titleTextView = itemViewGroup.findViewById<TextView>(R.id.text_title)
        val movieThumbImageView = itemViewGroup.findViewById<ImageView>(R.id.image_moviethumb)
        val ratingTextView = itemViewGroup.findViewById<TextView>(R.id.text_rating)

        val movie = MovieManager.savedMovies[position]

        titleTextView.text = movie.title
        val rating = movie.voteAverage
        ratingTextView.text = "Rating : $rating"
        val thumbURL = TmdbApi.getImageUrl(movie.imagePath)
        Picasso.get().load(thumbURL).into(movieThumbImageView)
    }

    inner class MovieViewHolder : RecyclerView.ViewHolder, View.OnClickListener {

        override fun onClick(view: View?) {
            val clickedPosition = adapterPosition
            listItemClickListener.onListItemClick(clickedPosition)
        }

        constructor(itemView : View) : super(itemView) {
            itemView.setOnClickListener(this)
        }
    }

}