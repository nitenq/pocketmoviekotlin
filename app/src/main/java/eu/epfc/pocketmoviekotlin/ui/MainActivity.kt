package eu.epfc.pocketmoviekotlin.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import eu.epfc.pocketmoviekotlin.model.MovieManager
import eu.epfc.pocketmoviekotlin.R

class MainActivity : AppCompatActivity() {

    inner class SimpleFragmentPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

        override fun getItem(position: Int): Fragment {
            return if (position == 0) {
                RecentMoviesFragment()
            } else{
                SavedMoviesFragment()
            }

        }

        override fun getCount(): Int {
            return 2
        }

        override fun getPageTitle(position: Int): CharSequence? {

            return if (position == 0){
                getString(R.string.pager_tab1name)
            } else {
                getString(R.string.pager_tab2name)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val viewPager : ViewPager = findViewById(R.id.pager)
        val simpleFragmentPagerAdapter = SimpleFragmentPagerAdapter(supportFragmentManager)
        viewPager.adapter = simpleFragmentPagerAdapter
        val tabLayout = findViewById<TabLayout>(R.id.sliding_tabs)
        tabLayout.setupWithViewPager(viewPager)

        // restore tab index
        if (savedInstanceState != null)
        {
            val tabIndex = savedInstanceState.getInt("tabIndex")
            viewPager.currentItem = tabIndex
        }
    }

    override fun onStart() {
        super.onStart()
        MovieManager.loadAllMoviesFromDatabase(applicationContext)
    }

    override fun onSaveInstanceState(outState: Bundle) {

        // save the current tab index
        super.onSaveInstanceState(outState)
        val tabLayout = findViewById<TabLayout>(R.id.sliding_tabs)
        outState.putInt("tabIndex", tabLayout.selectedTabPosition)
    }
}
