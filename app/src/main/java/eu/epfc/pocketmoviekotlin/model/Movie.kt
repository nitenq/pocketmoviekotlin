package eu.epfc.pocketmoviekotlin.model

import androidx.lifecycle.MutableLiveData
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity(tableName = "movies")
class Movie {

    @PrimaryKey
    val movieId : Int
    val title : String
    val imagePath : String
    val voteAverage : Float
    val releaseDate : String
    val overView : String
    @Ignore
    var details = MutableLiveData<MovieDetails?>()
    @Ignore
    var youtubeVideoId = MutableLiveData<String?>()

    constructor(movieId: Int, title: String, imagePath: String, voteAverage: Float, releaseDate: String, overView : String) {
        this.movieId = movieId
        this.title = title
        this.imagePath = imagePath
        this.voteAverage = voteAverage
        this.releaseDate = releaseDate
        this.overView = overView
    }

    inner class MovieDetails{

        val backdropPath : String
        val genreNames : List<String>

        constructor(backdropPath: String, genreNames: List<String>)
        {
            this.backdropPath = backdropPath
            this.genreNames = genreNames
        }
    }
}