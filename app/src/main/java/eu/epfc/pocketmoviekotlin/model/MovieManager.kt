package eu.epfc.pocketmoviekotlin.model

import android.content.Context
import androidx.lifecycle.MutableLiveData
import eu.epfc.pocketmoviekotlin.network.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


/**
 * Class handling http requests and database. This class owns the list of movies displayed in the UI
 */
object MovieManager {

    var popularMovies = MutableLiveData<List<Movie>>()
    var detailedMovieStatus = MutableLiveData<ConnectionResultStatus>()
    var youtubeIdStatus = MutableLiveData<ConnectionResultStatus>()
    var popularMoviesStatus = MutableLiveData<ConnectionResultStatus>()
    var savedMovies : List<Movie> = listOf()
    // current page of the popular movies
    var page = 1

    init {
        popularMovies.value = listOf()
        detailedMovieStatus.value = ConnectionResultStatus(ConnectionResultStatus.ConnectionResult.Uninitialized)
        popularMoviesStatus.value = ConnectionResultStatus(ConnectionResultStatus.ConnectionResult.Uninitialized)
    }

    /**
    Fetch next 20 movies from the server. Concatenate the new movies to the current movie list
     */
    fun fetchPopularMovies()
    {
        val retrofitCall : Call<RecentMovieResponse> = RetrofitClientInstance.getClient.getRecentMovies(page,TmdbApi.apiKey)

        val callback : Callback<RecentMovieResponse> = object : Callback<RecentMovieResponse>{
            override fun onFailure(call: Call<RecentMovieResponse>, t: Throwable) {
                popularMoviesStatus.value = ConnectionResultStatus(ConnectionResultStatus.ConnectionResult.ErrorNoConnection)
            }

            override fun onResponse(call: Call<RecentMovieResponse>, response: Response<RecentMovieResponse>) {
                val recentMovieResponse : RecentMovieResponse? = response.body()
                if (recentMovieResponse != null) {

                    val resultList : List<RecentMovieResponse.Result>? = recentMovieResponse.results
                    if (resultList != null) {

                        val newMovieList = popularMovies.value?.toMutableList()

                        for (result: RecentMovieResponse.Result in resultList) {

                            val newMovie = Movie(result.id ?: 0,
                                result.title ?: "",
                                result.posterPath ?: "",
                                result.voteAverage?.toFloat() ?: 0.0.toFloat(),
                                result.releaseDate ?: "",
                                result.overview ?: "")

                            newMovieList?.add(newMovie)
                        }
                        popularMovies.value = newMovieList
                        popularMoviesStatus.value = ConnectionResultStatus(ConnectionResultStatus.ConnectionResult.Success)
                        page++
                    }
                    else
                    {
                        popularMoviesStatus.value = ConnectionResultStatus(ConnectionResultStatus.ConnectionResult.ErrorServerInternal)
                    }
                }
            }
        }
        retrofitCall.enqueue(callback)
    }

    fun saveMovieInDatabase(movieId : Int, context : Context) {
        val movie : Movie? = getPopularMovieById(movieId)
        if (movie != null) {
            SavedMoviesDatabase.getInstance(context).savedMoviesDAO().addMovie(movie)
        }
    }

    fun removeMovieFromDatabase(movieId: Int, context: Context){
        val movie : Movie? = getPopularMovieById(movieId)
        if (movie != null) {
            SavedMoviesDatabase.getInstance(context).savedMoviesDAO().removeMovie(movie)
        }
    }

    fun loadAllMoviesFromDatabase(context: Context) {
         savedMovies = SavedMoviesDatabase.getInstance(context).savedMoviesDAO().allSavedMovies
    }


    /**
     * Launch an http request asynchronously to get details of a movie defined by movieId.
     * When response is received :
     * - the corresponding Movie object is filled with details coming from the response
     * - detailedMovieStatus is set to success or error
     */
    fun fetchDetailedMovie(movieId : Int) {

        val retrofitCall : Call<DetailMovieResponse> = RetrofitClientInstance.getClient.getDetailedMovie(movieId.toString(),TmdbApi.apiKey)

        val callback : Callback<DetailMovieResponse> = object : Callback<DetailMovieResponse>{
            override fun onFailure(call: Call<DetailMovieResponse>, t: Throwable) {
                detailedMovieStatus.value = ConnectionResultStatus(ConnectionResultStatus.ConnectionResult.ErrorNoConnection)
            }

            override fun onResponse(call: Call<DetailMovieResponse>, response: Response<DetailMovieResponse>) {

                val movie : Movie? = getPopularMovieById(movieId)
                if (movie != null){

                    // extract backdropPath and genres from the response, copy them to a new MovieDetails object
                    val backdropPath = response.body()?.backdropPath
                    val genres = response.body()?.genres
                    if (backdropPath != null && genres != null) {

                        var firstGenre = true
                        val genreNames : MutableList<String> = mutableListOf()
                        for (genre in genres) {
                            genre.name?.let { genreName ->
                                if (!firstGenre)
                                {
                                    genreNames.add(" - ")
                                }
                                genreNames.add(genreName)
                                firstGenre = false
                            }
                        }

                        val movieDetails = movie.MovieDetails(backdropPath, genreNames)
                        movie.details.value = movieDetails
                    }

                }
                detailedMovieStatus.value = ConnectionResultStatus(ConnectionResultStatus.ConnectionResult.Success)
            }
        }
        retrofitCall.enqueue(callback)
    }

    /**
     * Launch an http request asynchronously to get the youtube id of a movie defined by movieId.
     * When response is received :
     * - the corresponding Movie object is filled with the youtube id coming from the response
     * - youtubeIdStatus is set to success or error
     */
    fun fetchYoutubeVideoIds(movieId: Int){

        val retrofitCall : Call<MovieVideosResponse> = RetrofitClientInstance.getClient.getYoutubeVideoId(movieId.toString(),TmdbApi.apiKey)
        val callback : Callback<MovieVideosResponse> = object : Callback<MovieVideosResponse>{

            override fun onFailure(call: Call<MovieVideosResponse>, t: Throwable) {
                youtubeIdStatus.value = ConnectionResultStatus(ConnectionResultStatus.ConnectionResult.ErrorNoConnection)
            }

            override fun onResponse(call: Call<MovieVideosResponse>, response: Response<MovieVideosResponse>) {
                val movie : Movie? = getPopularMovieById(movieId)
                if (movie != null && response.body()?.results != null)
                {
                    val results : List<MovieVideosResponse.Result> = response.body()?.results!!
                    if (results.isNotEmpty()){

                        movie.youtubeVideoId.value = results[0].key
                    }
                    youtubeIdStatus.value = ConnectionResultStatus(ConnectionResultStatus.ConnectionResult.Success)

                }
            }
        }
        retrofitCall.enqueue(callback)
    }

    fun getPopularMovieById(movieId: Int): Movie? {

        val movieList : List<Movie>? = popularMovies.value
        return when ( movieList) {
            null -> null
            else -> movieList.find { it.movieId == movieId }
        }

    }

    /**
     * Return true if the movie is stored in the savedMovies list
     */
    fun isMovieSaved(movieId: Int): Boolean {

        val movieList : List<Movie>? = savedMovies
        return when ( movieList) {
            null -> false
            else -> movieList.any { it.movieId == movieId }
        }
    }
}