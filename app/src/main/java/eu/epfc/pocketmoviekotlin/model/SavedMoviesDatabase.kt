package eu.epfc.pocketmoviekotlin.model

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Movie::class], version = 1, exportSchema = false)
abstract class SavedMoviesDatabase : RoomDatabase() {

    abstract fun savedMoviesDAO(): SavedMoviesDAO

    companion object {

        private const val DATABASE_NAME = "pocket_movie_db"
        private var sInstance: SavedMoviesDatabase? = null

        fun getInstance(context: Context): SavedMoviesDatabase {
            if (sInstance == null) {

                val dbBuilder = Room.databaseBuilder(
                    context.applicationContext,
                    SavedMoviesDatabase::class.java,
                    DATABASE_NAME
                )
                dbBuilder.allowMainThreadQueries()
                sInstance = dbBuilder.build()

            }
            return sInstance!!
        }
    }
}