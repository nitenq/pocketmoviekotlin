package eu.epfc.pocketmoviekotlin.model

import androidx.room.*


@Dao
interface SavedMoviesDAO {

    @get:Query("SELECT * FROM movies")
    val allSavedMovies: List<Movie>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addMovie(movie: Movie)

    @Delete
    fun removeMovie(movie: Movie)

    @Query("SELECT * FROM movies WHERE movieId = :movieId")
    fun getMovie(movieId: Int): Movie
}